//
//  AccountTableViewCell.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 22/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import UIKit


//https://www.veasoftware.com/tutorials/2015/4/15/adding-buttons-to-table-view-cell-in-swift-xcode-63-ios-83-tutorial


class AccountTableViewCell: UITableViewCell {

    @IBOutlet var accountButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        accountButton.layer.borderWidth = 2.0
        accountButton.layer.cornerRadius = 10.0
        accountButton.layer.backgroundColor = UIColor.whiteColor().CGColor
        self.backgroundColor = UIColor.clearColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
