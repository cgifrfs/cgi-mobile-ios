//
//  AboutViewController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 09/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet var aboutWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "À propos..."
        
        
        var tryPage = NSURL(fileURLWithPath:(NSBundle.mainBundle().pathForResource("BU%20Financial%20Services%20-%20Innovation%202016%20-%20Contribution%20GFO%20v1.1", ofType:"pdf",inDirectory: nil))!)
        
        if (UIDevice.currentDevice().userInterfaceIdiom == .Pad)
        {
            tryPage = NSURL(fileURLWithPath:NSBundle.mainBundle().pathForResource("BU%20Financial%20Services%20-%20Innovation%202016%20-%20Contribution%20GFO%20v1.1", ofType:"pdf",inDirectory: nil)!)
        }
        
        let requestPage = NSURLRequest(URL: tryPage)
        
        aboutWebView.loadRequest(requestPage)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
