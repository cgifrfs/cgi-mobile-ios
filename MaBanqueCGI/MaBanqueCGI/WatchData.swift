//
//  WatchData.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 27/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import Foundation
import WatchKit
import WatchConnectivity

// http://stackoverflow.com/questions/27025526/passing-data-to-apple-watch-app

// Watch Session Manager

@available(iOS 9.0, *) public class WatchData: NSObject, WCSessionDelegate {

    var session = WCSession.defaultSession()
    var payload:String = ""

    class var shared: WatchData {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: WatchData? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = WatchData()
        }
        return Static.instance!
    }
    
    public func updateWatch(dataMessageLabel: String, dataObject: AnyObject) {
        
        //  WATCH
        // check the reachability
        if WCSession.defaultSession().reachable == false {
            print(" WATCH : WCSession unreachable")
            return
        }
        else{
            print(" WATCH : WCSession reachable => sendDataToWatch")
            WatchData.shared.sendDataToWatch(dataMessageLabel, data: dataObject)
        }
    }
    
    public func sessionReachabilityDidChange(session: WCSession){
        //print(__FUNCTION__)
        //print(session)
        print("reachability changed:\(session.reachable)")
        if (session.reachable)
        {
            print("\(__FUNCTION__) reachable")
        }
        else
        {
            print("\(__FUNCTION__) unreachable")
        }
    }
    
    public func sessionWatchStateDidChange(session: WCSession) {
        print(__FUNCTION__)
        print(session)
        print("sessionWatchStateDidChange reachable:\(session.reachable)")
    }
    
    public func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        //print(__FUNCTION__)
        guard message["request"] as? String == "showAlert" else {return}
        guard let m = message["m"] as? String else { return }
        print("\(__FUNCTION__) msg:",m)
    }
    
    
    public func sendDataToWatch(dataMessageLabel: String, data: AnyObject){
        print(__FUNCTION__)

        if (!session.reachable){
            if WCSession.isSupported() {    //  it is supported
                session = WCSession.defaultSession()
                session.delegate = self
                session.activateSession()
                print("iphone activating WCSession")
            } else {
                print("iphone does not support WCSession")
            }
            session.activateSession()
        }
        
        if(session.paired){
            if(session.watchAppInstalled){
                print("paired | watchAppInstalled")
            }
        }else{
            print("not paired | or no watchAppInstalled")
        }
        
        
        if(!session.reachable){
            print("not reachable")
            return
        }else{
            
            let transfer:WCSessionUserInfoTransfer =  (session.transferUserInfo(["data" : "Test2"]) as WCSessionUserInfoTransfer?)!
            if(transfer.transferring){
            print("-> iphone")
            }else{
            print("!-> iphone")
            }
            
            
            session.sendMessage([dataMessageLabel :data],
                replyHandler: { reply in
                    print("Reply : \(reply)")
                },
                errorHandler: { error in
                    print("Error : \(error)")
            })

        }
        
    }
    
}

