//
//  LoginViewController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 02/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//
// Chantier Innovation FS
// Alban NOGUÈS
// Ugo PARLANGE
// https://www.youtube.com/watch?v=w2eRgqt0ETk


import UIKit
import HealthKit
import WatchKit
import WatchConnectivity
import AVKit
import AVFoundation

//HealthStore

let storage = HKHealthStore()


let healthStore: HKHealthStore? = {
    if HKHealthStore.isHealthDataAvailable() {
        print(HKHealthStore())
        return HKHealthStore()
    } else {
        print("HKHealthStore == nil")
        return nil
    }
}()

var Timestamp: NSTimeInterval {
    return NSDate().timeIntervalSince1970 * 1000
}

var loginDelegate:LoginViewController? = nil


class LoginViewController: UIViewController, UITextFieldDelegate, WCSessionDelegate {

    @IBOutlet var validateButton: UIButton!
    @IBOutlet var login: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet weak var welcomeMessage: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    
    @IBOutlet var innoVideo: UIButton!
    
    let gradientLayer = CAGradientLayer()
    let buttonLayer = CAGradientLayer()
    
    var loginResult : String! = ""
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var strName = NSUserDefaults.standardUserDefaults().objectForKey("userName") as! String!
    var strCivility = NSUserDefaults.standardUserDefaults().objectForKey("userCivility") as! String!

    var avatarSize = Int()
    
    let heartRateType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
    let stepCountType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem?.title = " "
        
        
        // Gradient color background
        //http://blog.apoorvmote.com/gradient-background-uiview-ios-9-swift/?lang=fr
        gradientLayer.frame = self.view.bounds
        let color1 = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0).CGColor as CGColorRef
        let color2 = colorsCGI.nuageCGI.CGColor
        gradientLayer.colors = [color1, color2]
        gradientLayer.locations = [0.50, 1.0]
        self.view.layer.insertSublayer(gradientLayer, atIndex: 0)

        
        // Motion Effects initialization
        MotionFX.sharedInstance.setMotionFX(-30, max: 30)
        //innoVideo.addMotionEffect(MotionFX.sharedInstance.getMotionFX())
        
        //  WATCH : WCSessionDelegate
        if (WCSession.isSupported()) {
            let watchSession = WCSession.defaultSession()
            watchSession.delegate = self
            watchSession.activateSession()
        }
        
        //HealthKit
        
        if HKHealthStore.isHealthDataAvailable() {
        print("HealthStore is available")
        

            
            healthStore!.requestAuthorizationToShareTypes(nil, readTypes:[heartRateType, stepCountType!] , completion:{(success, error) in

            self.getHeartRate()

            //self.stepCounter()
            HealthKit().recentSteps() { steps, error in
                    // do something with steps
            }
            
            })
        }
        else {
        print("HealthStore is not available")
        }
        
        
        loginDelegate = self
        
        welcomeMessage.textColor = colorsCGI.nuageCGI
        
        login.keyboardAppearance = UIKeyboardAppearance.Dark
        password.keyboardAppearance = UIKeyboardAppearance.Dark
        
        login.textColor = colorsCGI.rougeCGI
        login.layer.borderWidth = 1.5
        login.layer.borderColor = colorsCGI.nuageCGI.CGColor
        login.layer.cornerRadius = 5.0
        
        password.textColor = colorsCGI.rougeCGI
        password.layer.borderWidth = 1.5
        password.layer.borderColor = colorsCGI.nuageCGI.CGColor
        password.layer.cornerRadius = 5.0
        
        userAvatar.layer.borderWidth = 2.0
        userAvatar.layer.borderColor = colorsCGI.nuageCGI.CGColor
        userAvatar.layer.cornerRadius = 2.0
        avatarSize = Int(userAvatar.bounds.width) * 3
        
        validateButton.setTitleColor(colorsCGI.nuageCGI, forState: UIControlState.Disabled)
        validateButton.enabled = true
        
        // Gradient button
        //http://blog.apoorvmote.com/gradient-background-uiview-ios-9-swift/?lang=fr
        buttonLayer.frame = self.validateButton.bounds
        buttonLayer.colors = [colorsCGI.rougeCGI.CGColor, colorsCGI.betteraveCGI.CGColor]
        buttonLayer.locations = [0.50, 1.0]
        buttonLayer.cornerRadius = 10.0
        validateButton.layer.insertSublayer(buttonLayer, atIndex: 0)
        
        

        phoneDataInit()

        welcomeMessage.text = "Veuillez vous identifier"
        userAvatar.hidden = true

        // Get the login in Settings
        if defaults.objectForKey("userIdentifier") != nil {
            login.text = defaults.objectForKey("userIdentifier") as! String!
            
            if defaults.objectForKey("userName") == nil {
                welcomeMessage.text = "Veuillez vous identifier"
                userAvatar.hidden = true
            }
            else
            {
                userAvatar.hidden = false
                welcomeMessage.text = "Bonjour \(strCivility) \(strName)!"
                loadAvatar("\(webSite.avatars)\(login.text!).png&size=\(avatarSize)")
            }
            
        }
        
     
        
        self.login.delegate = self
        self.password.delegate = self
        
    }
    
    @IBAction func validateAction(sender: UIButton) {
     
    
        // Login : https://fscgi.com/fwk/rest/session
        // Method : POST
        // Params : login, password, type (« CLI » ou « COL »)
        // Returns : Client ou null si problème identification
        
        // Logout
        // Method : DELETE
        
        //self.performSelectorOnMainThread("startSpinner:", withObject: nil, waitUntilDone: false)
        //startSpinner()
        self.startSpinner()
        
        if (login.text == "" && password.text == "")
        {
            showAlert("Veuillez saisir votre identifiant et votre mot de passe.")
            login.becomeFirstResponder()
            stopSpinner()
            return
        }
    
        if login.text == ""
        {
            showAlert("Veuillez saisir votre identifiant.")
            login.becomeFirstResponder()
            stopSpinner()
            return
        }
        
        if password.text == ""
        {
            showAlert("Veuillez saisir votre mot de passe.")
            password.becomeFirstResponder()
            stopSpinner()
            return
        }

        
        self.loginResult = ""
        
        
        self.validateButton.enabled = false
        
        //REST POST
        postDataToURL()

    }
    
    func phoneDataInit(){
        phoneDataExchange.logged = false
        phoneDataExchange.userName = ""
        phoneDataExchange.userCivility = ""
        phoneDataExchange.opportunities = ""
        phoneDataExchange.weatherDictionary = ["":""]
        //phoneDataExchange.heartRate = ""
        //phoneDataExchange.stepCount = ""
        //phoneDataExchange.accountsNamesAmounts.removeAllObjects()
        //GFO
        //phoneDataExchange.jsonAccountsData.delete(self)
        //phoneDataExchange.jsonAccounts
        //phoneDataExchange.jsonLoginData.delete(self)
    }
    
    
    func showAlert(messageToShow: String) {
        let alertController = UIAlertController(title: "⚠️ Attention", message:
            messageToShow, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)

    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == login {
            self.password.becomeFirstResponder()
        }

        if textField == password {
            self.login.becomeFirstResponder()
            validateAction(validateButton)
        }
        
        return true
    }
    

    
    // Dismissing the UITextField’s Keyboard
    // http://www.codingexplorer.com/how-to-dismiss-uitextfields-keyboard-in-your-swift-app/
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
    

    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == login{
            welcomeMessage.text = "Veuillez vous identifier"
            userAvatar.hidden = true

        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == login{
            if textField.text == defaults.objectForKey("userIdentifier") as! String!{
                welcomeMessage.text = "Bonjour \(strCivility) \(strName)!"
                userAvatar.hidden = false
            }
            //update avatar
            loadAvatar("\(webSite.avatars)\(login.text!).png&size=\(avatarSize)")
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        //Load data
        /*
        let about1 = "Crédit : \n\(defaults.objectForKey("appCopyright") as! String!)"
        let about2 = "Entité : \n\(defaults.objectForKey("appCredit") as! String!)"
        let about3 = "Projet : \n\(defaults.objectForKey("appBU") as! String!)"
        let about4 = "Version : \n\(defaults.objectForKey("appProject") as! String!)"
        let about5 = "Date de mise à jour : \n\(defaults.objectForKey("appVersion") as! String!)"
        let about6 = "Responsable projet : \n\(defaults.objectForKey("directedBy") as! String!)"
        let about7 = "Supervision : \n\(defaults.objectForKey("supervisedBy") as! String!)"
        let about8 = "Développement : \n\(defaults.objectForKey("programedBy") as! String!)"
        */
        
        /*
        // ===== BOUCHON ========
        let about0 = "Ma Banque CGI"
        let about1 = "Crédit : © 2015 \nGroupe CGI inc."
        let about2 = "Entité : \nFinancial Services"
        let about3 = "Projet : \nInnovation 2016"
        let about4 = "Version : \n1.0 Beta"
        let about5 = "Date de màj : \nDécembre 2015"
        let about6 = "Responsable projet : \nAlban NOGUÈS"
        let about7 = "Supervision : \nUgo PARLANGE"
        let about8 = "Développement : \nGhislain FORTIN"

        phoneDataExchange.about = "\(about0)\n\n\(about1)\n\n\(about2)\n\n\(about3)\n\n\(about4)\n\n\(about5)\n\n\(about6)\n\n\(about7)\n\n\(about8)"
        print(phoneDataExchange.about)
        
        WatchData.shared.updateWatch("about", dataObject: phoneDataExchange.about)
        */
        
        if segue.identifier == "showSummary" {
            let destinationVC = segue.destinationViewController as! SummaryViewController
            destinationVC.login = login.text!
        }
        
        if segue.identifier == "showVideo" {
        let destination = segue.destinationViewController as! AVPlayerViewController
        //let url = NSURL(string:"https://www.youtube.com/watch?v=w2eRgqt0ETk")
        let url = NSBundle.mainBundle().URLForResource("MaBanqueCGI-960x540", withExtension: "mp4")
        destination.player = AVPlayer(URL: url!)
            
        }
        
    }
    

    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
        //http://stackoverflow.com/questions/12561735/what-are-unwind-segues-for-and-how-do-you-use-them
        
        //login.text = ""
        login.text = defaults.objectForKey("userIdentifier") as! String!
        password.text = ""
        strName = NSUserDefaults.standardUserDefaults().objectForKey("userName") as! String!
        strCivility = NSUserDefaults.standardUserDefaults().objectForKey("userCivility") as! String!
        welcomeMessage.text = "Bonjour \(strCivility) \(strName)!"
        //userName.text = "Veuillez vous identifier"
        userAvatar.hidden = false
        
    }
    
    
    func postDataToURL() {
                
        // Setup the session to make REST POST call
        let url = NSURL(string: webSite.session)!
        
        let session = NSURLSession.sharedSession()
        let postParams : [String: String!] = ["login": login.text, "password": password.text, "type": "CLI"]
        
        // Create the request
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(postParams, options: NSJSONWritingOptions())
            //print(postParams)
        } catch {
            print("bad things happened")
        }
        
        // Make the POST call and handle it in a completion handler
        let task = session.dataTaskWithRequest(request, completionHandler: { ( data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            //print(response)
            
            // Make sure we get an OK response
            guard let realResponse = response as? NSHTTPURLResponse where
                realResponse.statusCode == 200 else {
                    print("Not a 200 response")
                    return
            }
            
            //print("Task error : \(error)")
            
            //print("NSData : \(data)")
            
            // Read the JSON
            if let postString = NSString(data:data!, encoding: NSUTF8StringEncoding) as? String {
                // Print what we got from the call
                 //print("POST: " + postString)
                
                
                self.performSelectorOnMainThread("loginParseJSON:", withObject: data, waitUntilDone: true)
                
                self.loginResult = postString
                self.performSelectorOnMainThread("updateLoginResult:", withObject: postString, waitUntilDone: false)
                


            }
            
        })
            
        task.resume()
    }
    
    
    
    //MARK: - Method to update the result immediately on the Main Thread
    func loginParseJSON(data: NSData) {
        

        // Read the JSON
        do {
            if let loginString = NSString(data:data, encoding: NSUTF8StringEncoding) {

                // Share the data from the iPhone to the WATCH
                phoneDataExchange.jsonLoginData = data
                WatchData.shared.updateWatch("jsonLoginData", dataObject: phoneDataExchange.jsonLoginData)
                
                // Print what we got from the call
                print("loginString :  \(loginString)")
                
                // Parse the JSON to get the Login detail
                let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                
                let userCivility: NSDictionary! = jsonDictionary["civility"]! as! NSDictionary

                //print(userCivility["label"]!)
                
                // Preferences : save user name and firstname in Settings
                //let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(jsonDictionary["firstName"]!, forKey: "userFirstName")
                defaults.setObject(jsonDictionary["lastName"]!, forKey: "userName")
                defaults.setObject(userCivility["label"]!, forKey: "userCivility")
                defaults.synchronize()
                
                phoneDataExchange.logged = true
                phoneDataExchange.userName = jsonDictionary["lastName"]! as! String
                phoneDataExchange.userCivility = userCivility["label"]! as! String
                
                
                let lastPosition: NSDictionary! = jsonDictionary["lastPosition"]! as! NSDictionary

                
                print("Last Position : \(lastPosition)")
                if lastPosition.count == 0
                {
                    phoneDataExchange.lastPosDesc = ""
                    phoneDataExchange.lastPosLat = 0.000
                    phoneDataExchange.lastPosLong = 0.000
                }
                else
                {
                    if lastPosition["description"] is NSNull
                    {
                        phoneDataExchange.lastPosDesc = ""
                    }
                    else
                    {
                        phoneDataExchange.lastPosDesc = lastPosition["description"]! as! String
                    }
                    phoneDataExchange.lastPosLat = lastPosition["latitude"]! as! Double
                    phoneDataExchange.lastPosLong = lastPosition["longitude"]! as! Double
                }
                    
                    
                phoneDataExchange.addressesArray = jsonDictionary["addresses"]! as! NSArray
                
                if phoneDataExchange.addressesArray.count == 0
                {
                    phoneDataExchange.addressName = ""
                    phoneDataExchange.addressDesc = ""
                    phoneDataExchange.addressLat = 0.000
                    phoneDataExchange.addressLong = 0.000
                    
                }
                else
                {
                    let address: NSDictionary = phoneDataExchange.addressesArray[0] as! NSDictionary
                    phoneDataExchange.addressName = address["name"]! as! String
                    phoneDataExchange.addressDesc = address["description"]! as! String
                    phoneDataExchange.addressLat = address["latitude"]! as! Double
                    phoneDataExchange.addressLong = address["longitude"]! as! Double
                }
                
                
                

                //print("Nombre d'attributs du JSON login : \(jsonDictionary.count)")
                //print (loginString)
                //print (jsonDictionary["firstName"]!)
                //print (jsonDictionary["lastName"]!)
                

                //print ("Opportunities : \(jsonDictionary["opportunities"]!)")
                phoneDataExchange.opportunitiesArray = jsonDictionary["opportunities"]! as! NSArray
              
                //print ("phoneDataExchange.opportunitiesArray : \(phoneDataExchange.opportunitiesArray)")

                
                if phoneDataExchange.opportunitiesArray.count == 0 {
                    phoneDataExchange.opportunities = ""
                }
                else {
                        let oppName: NSDictionary = phoneDataExchange.opportunitiesArray[0] as! NSDictionary
                        phoneDataExchange.opportunities = oppName["name"]! as! String
                        //print("phoneDataExchange.opportunities : \(phoneDataExchange.opportunities)")
                }
                
            }
        } catch {
            print("bad things happened")
        }
    }


    //MARK: - Method to update the result immediately on the Main Thread
    func updateLoginResult(result: String) {
        self.loginResult = result
        
        self.validateButton.enabled = true
        
        if self.loginResult != "" {
            
            self.performSegueWithIdentifier("showSummary", sender: self)
            
            // Preferences update in Settings
            //let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(self.login.text, forKey: "userIdentifier")
            defaults.synchronize()
        }
        else
        {
            showAlert("Problème d'authentification! Veuillez réessayer de nouveau.")
            password.text = ""
            password.becomeFirstResponder()
            self.stopSpinner()
            return
        }
        
        // Parse the JSON to get the Accounts detail
        //let jsonArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSArray
        
        
    }


    func loadAvatar(urlString:String)
    {
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            
            // Make sure we get an OK response
            guard let realResponse = response as? NSHTTPURLResponse where
                realResponse.statusCode == 200 else {
                    print("Status code != 200 : Erreur de récupération de l'avatar")
                    return
            }
            
            if error == nil {
                // Update Avatar on Main Thread
                self.performSelectorOnMainThread("updateAvatarImage:", withObject: data, waitUntilDone: false)
            }
            else
            {
                print("Erreur de récupération de l'avatar")
            }
        })
        task.resume()
        
    }

    //MARK: - Methods to update the UI immediately
    func updateAvatarImage(imageData: NSData) {
        
        AvatarImage.sharedInstance.setImage(UIImage(data: imageData)!)
        phoneDataExchange.avatarImageData = imageData
        WatchData.shared.updateWatch("avatarImage", dataObject: phoneDataExchange.avatarImageData)
        self.userAvatar.image = AvatarImage.sharedInstance.getImage()
        
    }

    
    func startSpinner(){
        (UIApplication.sharedApplication().delegate as! AppDelegate).setNetworkActivityIndicatorVisible(true)
    }
    
    func stopSpinner(){
        (UIApplication.sharedApplication().delegate as! AppDelegate).setNetworkActivityIndicatorVisible(false)
    }

    
    private func getHeartRate(){
        
        if (HKHealthStore.isHealthDataAvailable()){
            
            var csvString = "Time,Date,Heartrate(BPM)\n"
            
            let sortByTime = NSSortDescriptor(key:HKSampleSortIdentifierEndDate, ascending:false)
            let timeFormatter = NSDateFormatter()
            timeFormatter.dateFormat = "hh:mm:ss"
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd/MM/YYYY"
            
            let query = HKSampleQuery(sampleType:heartRateType, predicate:nil, limit:1, sortDescriptors:[sortByTime], resultsHandler:{(query, results, error) in
                guard let results = results else { return }
                for quantitySample in results {
                    let quantity = (quantitySample as! HKQuantitySample).quantity
                    let heartRateUnit = HKUnit(fromString: "count/min")
                    
                    
                    csvString += "\(timeFormatter.stringFromDate(quantitySample.startDate)),\(dateFormatter.stringFromDate(quantitySample.startDate)),\(quantity.doubleValueForUnit(heartRateUnit))\n"
                    
                    //phoneDataExchange.heartRate = String(csvString)
                    phoneDataExchange.heartRate = String(Int(quantity.doubleValueForUnit(heartRateUnit)))
                    
                    WatchData.shared.updateWatch("heartRate", dataObject:  phoneDataExchange.heartRate)

                    
                    print("\(timeFormatter.stringFromDate(quantitySample.startDate)), \(dateFormatter.stringFromDate(quantitySample.startDate)), \(quantity.doubleValueForUnit(heartRateUnit))")
                }
                
                do {
                    let documentsDir = try NSFileManager.defaultManager().URLForDirectory(.DocumentDirectory, inDomain:.UserDomainMask, appropriateForURL:nil, create:true)
                    try csvString.writeToURL(NSURL(string:"heartratedata.csv", relativeToURL:documentsDir)!, atomically:true, encoding:NSASCIIStringEncoding)
                }
                catch {
                    print("Error occured")
                }
                
            })
            
            print("HealthStore heart rate query result :")
            healthStore!.executeQuery(query)
        }
        
    }
    
    /*
    func stepCounter() {
        //http://stackoverflow.com/questions/28802053/healthkit-step-counter
        
        let endDate = NSDate()
        let startDate = NSCalendar.currentCalendar().dateByAddingUnit(.Month, value: -1, toDate: endDate, options: .MatchFirst)
        
        //let weightSampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)
        let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: .None)
        
        let query = HKSampleQuery(sampleType: stepCountType!, predicate: predicate, limit: 1, sortDescriptors: nil, resultsHandler: {
            (query, results, error) in
            if results == nil {
                print("There was an error running the query: \(error)")
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                var dailyAVG:Double! = 0
                for steps in results as! ([HKQuantitySample])!
                {
                    // add values to dailyAVG
                    dailyAVG = dailyAVG + steps.quantity.doubleValueForUnit(HKUnit.countUnit())
                    print(dailyAVG)
                    print(steps)
                }
            }
        
        healthStore?.executeQuery(query)
        })
    }
    */
    

    //https://swift.unicorn.tv/articles/step-counter-in-healthkit
    class HealthKit
    {
        let storage = HKHealthStore()
        
        init()
        {
            print("HealthKit class")
            checkAuthorization()
        }
        
        func checkAuthorization() -> Bool
        {

            // Default to assuming that we're authorized
            var isEnabled = true
            
            // Do we have access to HealthKit on this device?
            if HKHealthStore.isHealthDataAvailable()
            {
                // We have to request each data type explicitly
                let steps = NSSet(object: HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!)
                
                // Now we can request authorization for step count data
                storage.requestAuthorizationToShareTypes(nil, readTypes: steps as? Set<HKObjectType>) { (success, error) -> Void in
                    isEnabled = success
                }
            }
            else
            {
                print("isEnabled = false")

                isEnabled = false
            }
            
            return isEnabled
        }
        
        func recentSteps(completion: (Double, NSError?) -> () )
        {
            // The type of data we are requesting (this is redundant and could probably be an enumeration
            let type = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)

            let startDate = NSCalendar.currentCalendar().dateByAddingUnit(.Month, value: -1, toDate: NSDate(), options: .MatchFirst)

            // Our search predicate which will fetch data from now until a day ago
            let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: NSDate(), options: .None)
            
            // The actual HealthKit Query which will fetch all of the steps and sub them up for us.
            let query = HKSampleQuery(sampleType: type!, predicate: predicate, limit: 0, sortDescriptors: nil) { query, results, error in
                var steps: Double = 0
                
                if results?.count > 0
                {
                    for result in results as! [HKQuantitySample]
                    {
                        steps += result.quantity.doubleValueForUnit(HKUnit.countUnit())
                    }
                }
                
                print("steps : \(Int(steps)) this month")
                
                phoneDataExchange.stepCount = String(Int(steps))
                WatchData.shared.updateWatch("stepCount", dataObject:  phoneDataExchange.stepCount)

                
                completion(steps, error)
            }
            
            storage.executeQuery(query)
        }
        
    }

}


