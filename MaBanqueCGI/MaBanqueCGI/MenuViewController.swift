//
//  MenuViewController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 08/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation



class MenuViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    let locationManager = CLLocationManager()
    var currentLocationAddress: String = ""


    @IBOutlet var summaryButton: UIButton!
    
    @IBOutlet var geolocationButton: UIButton!
    
    @IBOutlet var geolocationSwitch: UISwitch!
    
    @IBOutlet var currentLocation: UILabel!
    @IBOutlet var settingsButton: UIButton!

    @IBOutlet var mapView: MKMapView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set map view delegate with controller
        self.mapView.delegate = self
        
        
        // Desactivate Summary menu
        summaryButton.enabled = false
        summaryButton.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Disabled)


        currentLocation.text = ""
        currentLocationAddress = ""
        
        if phoneDataExchange.addressName == "Maison" {
            phoneDataExchange.addressName = "🏠 \(phoneDataExchange.addressName)"
        }
        
        
        if phoneDataExchange.addressLat == 0 {
            print("No address to pin")
        }
        else
        {
            // Pin address
            let pinAddress = MKPointAnnotation()
            pinAddress.coordinate = CLLocationCoordinate2DMake(phoneDataExchange.addressLat, phoneDataExchange.addressLong)
            pinAddress.title = "\(phoneDataExchange.addressName) de \(phoneDataExchange.userCivility) \(phoneDataExchange.userName)"
            pinAddress.subtitle = phoneDataExchange.addressDesc
            self.mapView.addAnnotation(pinAddress)
            
            // Set region of the map showed
            let center = CLLocationCoordinate2D(latitude: phoneDataExchange.addressLat, longitude: phoneDataExchange.addressLong)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
            self.mapView.setRegion(region, animated: true)
        }
        
        if phoneDataExchange.lastPosLat == 0
        {
            print("No last position to pin")
        }
        else
        {
            // Pin last position
            let pinLastPosition = MKPointAnnotation()
            pinLastPosition.coordinate = CLLocationCoordinate2DMake(phoneDataExchange.lastPosLat, phoneDataExchange.lastPosLong)
            pinLastPosition.title = "🏫 Dernière position enregistrée"
            pinLastPosition.subtitle = phoneDataExchange.lastPosDesc
            self.mapView.addAnnotation(pinLastPosition)
    
            // Set region of the map showed
            let center = CLLocationCoordinate2D(latitude: phoneDataExchange.lastPosLat, longitude: phoneDataExchange.lastPosLong)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
            self.mapView.setRegion(region, animated: true)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func settingsAction(sender: AnyObject) {
        // Open phone settings
        UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)

    }
    

    @IBAction func logoutAction(sender: AnyObject) {
        //print("logout action")
        postDataToURLRESTDelete()
    
    }

    @IBAction func summaryAction(sender: AnyObject) {
        
        performSegueWithIdentifier("unwindToSummaryID", sender: self)
        
    }
    
    @IBAction func geolocationAction(sender: AnyObject) {
        
        if geolocationSwitch.on
        {
            geolocationSwitch.setOn(false, animated: true)
        }
        else
        {
            geolocationSwitch.setOn(true, animated: true)
        }
        
        geolocSwitchAction(self)
        
    }
    

    @IBAction func geolocSwitchAction(sender: AnyObject) {
        
        //var alertMessage = String()
                
        if geolocationSwitch.on
        {
            //Active la géolocalisation
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestLocation()
            locationManager.startUpdatingLocation()
            
        }
        else
        {
            //Désactive la géolocalisation
            locationManager.stopUpdatingLocation()
            currentLocation.text = ""
            currentLocationAddress = ""
        }

    }


    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func postDataToURLRESTDelete() {
        
        // Setup the session to make REST DELETE call
        let postEndpoint: String = "https://fscgi.com/fwk/rest/session"
        let url = NSURL(string: postEndpoint)!
        let session = NSURLSession.sharedSession()
        let postParams : [String: String!] = ["login": "", "password": "", "type": ""]
        //let postParams  = ""
        
        
        // Create the request
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "DELETE"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(postParams, options: NSJSONWritingOptions())
            //print(postParams)
        } catch {
            print("bad things happened")
        }
        
        // Make the POST call and handle it in a completion handler
        session.dataTaskWithRequest(request, completionHandler: { ( data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // Make sure we get an OK response
            guard let realResponse = response as? NSHTTPURLResponse where
                realResponse.statusCode == 200 else {
                    print("Not a 200 response")
                    return
            }
            
            // Read the JSON
            if let postString = NSString(data:data!, encoding: NSUTF8StringEncoding) as? String {
                // Print what we got from the call
                // print("POST: " + postString)
                //self.loginResult = postString
                self.performSelectorOnMainThread("updateLoginResult:", withObject: postString, waitUntilDone: false)
            }
            
        }).resume()
    }
    
    //MARK: - Method to update the result immediately on the Main Thread
    func updateLoginResult(result: String) {
        print("==> Logout")
        
        loginDelegate?.phoneDataInit()
       // WatchData.shared.sendDataToWatch(phoneDataExchange.opportunities, jsonAccountsString: phoneDataExchange.jsonAccountsString)

        WatchData.shared.sendDataToWatch("connection", data: false)
        //WatchData.shared.sendDataToWatch("opportunities",data: "")
        //WatchData.shared.sendDataToWatch("jsonArrayData", data: "")
        
        //self.loginResult = result
    }
    
    
    
    // Ne pas oublier d'ajouter NSLocationWhenInUseUsageDescription dans info.plist
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)-> Void in

        if (error != nil) {
            print("Reverse geocoder failed with error" + error!.localizedDescription)
            return
        }
        
        
        if placemarks!.count != 0 {
            let pm = placemarks![0] as CLPlacemark
            self.displayLocationInfo(pm)
            
            let location = locations.last as CLLocation!
            
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            self.mapView.setRegion(region, animated: true)

            
            // Show an animated dot at the current location
            self.mapView.userLocation.title = "Position actuelle :"
            self.mapView.userLocation.subtitle = self.currentLocationAddress
            self.mapView.showsUserLocation = true
            

            
            /*
            //Pin actual location
            
            let pinLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            
            var Annotations = [MKAnnotation]()
            let objectAnnotation = MKPointAnnotation()
            objectAnnotation.coordinate = pinLocation
            objectAnnotation.title = "Vous êtes ici!"
            //self.mapView.addAnnotation(objectAnnotation)
            Annotations.append(objectAnnotation)
            self.mapView.showAnnotations(Annotations, animated: true)
            */
            
        } else {
            print("Problem with the data received from geocoder")
        }
            
            
            
    })
    }
    
    
    func displayLocationInfo(placemark: CLPlacemark!) {
        if placemark != nil {
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
            
            if (placemark.locality != nil){
                print(placemark.locality!)
            }
            if (placemark.postalCode != nil){
                print(placemark.postalCode!)
            }
            if (placemark.administrativeArea != nil){
                print(placemark.administrativeArea!)
            }
            if (placemark.country != nil){
                print(placemark.country!)
                currentLocationAddress = "\(placemark.subThoroughfare!), \(placemark.thoroughfare!)"
                currentLocation.text = "\(placemark.locality!) \(placemark.postalCode!), \(placemark.administrativeArea!) (\(placemark.country!))"
            }
            
            
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)

    }
    
/*
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
            if (annotation is MKUserLocation) {
                return nil
            }
            
            if (annotation.isKindOfClass(CustomAnnotation)) {
                let customAnnotation = annotation as? CustomAnnotation
                mapView.translatesAutoresizingMaskIntoConstraints = false
                var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("CustomAnnotation") as MKAnnotationView!
                
                if (annotationView == nil) {
                    annotationView = customAnnotation?.annotationView()
                } else {
                    annotationView.annotation = annotation;
                }
                
                self.addBounceAnimationToView(annotationView)
                return annotationView
            } else {
                return nil
            }
        }
*/
  
/*
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        // This is false if its a user pin
        if(annotation.isKindOfClass(CustomAnnotation) == false)
        {
            let userPin = "userLocation"
            if let dequeuedView = _view.mapView().dequeueReusableAnnotationViewWithIdentifier(userPin)
            {
                return dequeuedView
            } else
            {
                let mkAnnotationView:MKAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: userPin)
                mkAnnotationView.image = UIImage(named: C_GPS.ROUTE_WALK_ICON_NAME)
                let offset:CGPoint = CGPoint(x: 0, y: -mkAnnotationView.image!.size.height / 2)
                mkAnnotationView.centerOffset = offset
                
                return mkAnnotationView
            }
            
        }
        
        let annotation = annotation as? CustomAnnotation
        if(annotation == nil)
        {
            return nil
        }
        
        let endPointsIdentifier = "endPoint"
        if let dequeuedView = _view.mapView().dequeueReusableAnnotationViewWithIdentifier(endPointsIdentifier)
        {
            dequeuedView.image = annotation!.uiimage
            return dequeuedView
        } else
        {
            let mkAnnotationView:MKAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: endPointsIdentifier)
            mkAnnotationView.image = annotation!.uiimage
            let offset:CGPoint = CGPoint(x: 0, y: -mkAnnotationView.image!.size.height / 2)
            mkAnnotationView.centerOffset = offset
            
            let gesture = UITapGestureRecognizer(target: self, action: "routeTouched:")
            mkAnnotationView.addGestureRecognizer(gesture)
            
            return mkAnnotationView
        }
    }
   */
    
    
}
