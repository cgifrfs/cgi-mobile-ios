//
//  SimulationViewController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 11/02/2016.
//  Copyright © 2016 CGI-FS. All rights reserved.
//

import UIKit

class SimulationViewController: UIViewController {

    @IBOutlet var productsPickerView: UIPickerView!
    @IBOutlet var simulationAmount: UITextField!
 
    @IBOutlet var firstAccountName: UILabel!
    @IBOutlet var firstAccountAmount: UITextField!
    @IBOutlet var firstSlider: UISlider!
    
    @IBOutlet var secondAccountName: UILabel!
    @IBOutlet var secondAccountAmount: UITextField!
    @IBOutlet var secondSlider: UISlider!
    
    @IBOutlet var thirdAccountName: UILabel!
    @IBOutlet var thirdAccountAmount: UITextField!
    @IBOutlet var thirdSlider: UISlider!
    
    @IBOutlet var submitButton: UIButton!
    
    @IBOutlet var background: UIImageView!
    
    let gradientLayer = CAGradientLayer()
    let buttonLayer = CAGradientLayer()

    
    var products = ["Assurance vie","Assurance invalidité","Épargne logement"]
    var firstAccount: Float = 0.0
    var secondAccount: Float = 0.0
    var thirdAccount: Float = 0.0

    let colorProduct = colorsCGI.rougeCGI
    let colorAccount1 = colorsCGI.citrouilleCGI
    let colorAccount2 = colorsCGI.betteraveCGI
    let colorAccount3 = colorsCGI.mielCGI
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Gradient color background
        //http://blog.apoorvmote.com/gradient-background-uiview-ios-9-swift/?lang=fr
        gradientLayer.frame = self.view.bounds
        let color1 = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0).CGColor as CGColorRef
        let color2 = colorsCGI.nuageCGI.CGColor
        gradientLayer.colors = [color1, color2]
        gradientLayer.locations = [0.50, 1.0]
        self.view.layer.insertSublayer(gradientLayer, atIndex: 0)
        
        background.addMotionEffect(MotionFX.sharedInstance.getMotionFX())

        
        submitButton.layer.cornerRadius = 10.0
        submitButton.setTitleColor(colorsCGI.nuageCGI, forState: UIControlState.Disabled)
        buttonLayer.frame = self.submitButton.bounds
        buttonLayer.colors = [colorsCGI.rougeCGI.CGColor, colorsCGI.betteraveCGI.CGColor]
        buttonLayer.locations = [0.50, 1.0]
        buttonLayer.cornerRadius = 10.0
        submitButton.layer.insertSublayer(buttonLayer, atIndex: 0)

        simulationAmount.keyboardAppearance = UIKeyboardAppearance.Dark
        simulationAmount.enabled = false
        simulationAmount.layer.borderColor = colorProduct.CGColor
        simulationAmount.layer.cornerRadius = 10.0
        simulationAmount.layer.borderWidth = 2.0
        simulationAmount.layer.backgroundColor = UIColor.whiteColor().CGColor
        
        firstAccountAmount.enabled = false
        firstAccountAmount.hidden = true
        firstAccountAmount.keyboardAppearance = UIKeyboardAppearance.Dark
        
        firstAccountAmount.layer.borderColor = colorAccount1.CGColor
        firstAccountAmount.layer.cornerRadius = 10.0
        firstAccountAmount.layer.borderWidth = 2.0
        firstAccountAmount.layer.backgroundColor = UIColor.whiteColor().CGColor
        
        secondAccountAmount.layer.borderColor = colorAccount2.CGColor
        secondAccountAmount.layer.cornerRadius = 10.0
        secondAccountAmount.layer.borderWidth = 2.0
        secondAccountAmount.layer.backgroundColor = UIColor.whiteColor().CGColor
        
        thirdAccountAmount.layer.borderColor = colorAccount3.CGColor
        thirdAccountAmount.layer.cornerRadius = 10.0
        thirdAccountAmount.layer.borderWidth = 2.0
        thirdAccountAmount.layer.backgroundColor = UIColor.whiteColor().CGColor
        
        firstAccountName.hidden = true
        firstSlider.hidden = true
        secondAccountAmount.enabled = false
        secondAccountAmount.hidden = true
        secondAccountAmount.keyboardAppearance = UIKeyboardAppearance.Dark
        secondAccountName.hidden = true
        secondSlider.hidden = true
        thirdAccountAmount.enabled = false
        thirdAccountAmount.hidden = true
        thirdAccountAmount.keyboardAppearance = UIKeyboardAppearance.Dark
        thirdAccountName.hidden = true
        thirdSlider.hidden = true

        
        //Bouchons
        
        /*
        if phoneDataExchange.jsonAccounts.count != 0
        {
            for account in phoneDataExchange.jsonAccounts {
                let accountLabel : String! = account["label"] as! String!
                let accountBalance : NSNumber! = account["balance"] as! NSNumber!
            }
        }
        */
        
        if phoneDataExchange.jsonAccounts.count != 0 {
            firstAccountName.text = phoneDataExchange.jsonAccounts[0].valueForKey("label") as? String
            self.firstAccount = (phoneDataExchange.jsonAccounts[0].valueForKey("balance") as? Float)!
            firstAccountAmount.hidden = false
            firstAccountName.hidden = false
            firstSlider.hidden = false
            if phoneDataExchange.jsonAccounts.count > 1 {
                secondAccountName.text =  phoneDataExchange.jsonAccounts[1].valueForKey("label") as? String
                self.secondAccount = (phoneDataExchange.jsonAccounts[1].valueForKey("balance") as? Float)!
                secondAccountAmount.hidden = false
                secondAccountName.hidden = false
                secondSlider.hidden = false
                if phoneDataExchange.jsonAccounts.count > 2 {
                    secondAccountName.text =  phoneDataExchange.jsonAccounts[2].valueForKey("label") as? String
                    self.secondAccount = (phoneDataExchange.jsonAccounts[2].valueForKey("balance") as? Float)!
                    thirdAccountAmount.hidden = false
                    thirdAccountName.hidden = false
                    thirdSlider.hidden = false
                }
            }
        }
        


        
        simulationAmount.textColor = colorProduct
        
        firstSlider.minimumValue = 0
        firstSlider.maximumValue = firstAccount
        firstSlider.value = firstAccount
        firstAccountName.textColor = colorAccount1
        firstAccountAmount.textColor = colorAccount1
        firstSlider.minimumTrackTintColor = colorAccount1
        firstSlider.maximumTrackTintColor = colorProduct
        
        secondSlider.minimumValue = 0
        secondSlider.maximumValue = secondAccount
        secondSlider.value = secondAccount
        secondAccountName.textColor = colorAccount2
        secondAccountAmount.textColor = colorAccount2
        secondSlider.minimumTrackTintColor = colorAccount2
        secondSlider.maximumTrackTintColor = colorProduct

        thirdSlider.minimumValue = 0
        thirdSlider.maximumValue = thirdAccount
        thirdSlider.value = thirdAccount
        thirdAccountName.textColor = colorAccount3
        thirdAccountAmount.textColor = colorAccount3
        thirdSlider.minimumTrackTintColor = colorAccount3
        thirdSlider.maximumTrackTintColor = colorProduct

        
        firstAccountAmount.text = AmountFormat.sharedInstance.currencyFormatter(firstSlider.value) as String
        secondAccountAmount.text = AmountFormat.sharedInstance.currencyFormatter(secondSlider.value) as String
        thirdAccountAmount.text = AmountFormat.sharedInstance.currencyFormatter(thirdSlider.value) as String
        
        calculateProduct()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func slider1Action(sender: AnyObject) {
        firstAccountAmount.text = AmountFormat.sharedInstance.currencyFormatter(firstSlider.value) as String
        calculateProduct()
    }
    
    @IBAction func slider2Action(sender: AnyObject) {
        secondAccountAmount.text = AmountFormat.sharedInstance.currencyFormatter(secondSlider.value) as String
        calculateProduct()

    }
    
    @IBAction func slider3Action(sender: AnyObject) {
        thirdAccountAmount.text = AmountFormat.sharedInstance.currencyFormatter(thirdSlider.value) as String
        calculateProduct()

    }
    
    func calculateProduct() {
        let sumAccounts = firstAccount + secondAccount + thirdAccount
        let sumProduct = sumAccounts - firstSlider.value - secondSlider.value - thirdSlider.value
        simulationAmount.text = AmountFormat.sharedInstance.currencyFormatter(sumProduct) as String
        
        if sumProduct == 0 {
            submitButton.enabled = false
        }
        else
        {
            submitButton.enabled = true
        }
    }
    
    
    @IBAction func submitAction(sender: AnyObject) {
    
        let alertController = UIAlertController(title: "Souscription", message:
            "⚙ Transmission en cours...", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    //http://www.ioscreator.com/tutorials/picker-view-tutorial-ios8-swift
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return products.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return products[row]
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        let titleData = products[row]

        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(17.0),NSForegroundColorAttributeName:self.colorProduct])
        pickerLabel.attributedText = myTitle
        return pickerLabel
    }
    
    // Dismissing the UITextField’s Keyboard
    // http://www.codingexplorer.com/how-to-dismiss-uitextfields-keyboard-in-your-swift-app/
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == firstAccountAmount{
            
            
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == firstAccountAmount{


        }
    }
    
}
