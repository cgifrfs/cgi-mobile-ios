//
//  AccountTableViewController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 07/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import UIKit

class AccountTableViewController: UITableViewController {

    var accountTitle = String()
    var accountColor = UIColor()
    var accountOperations = NSArray()
    var operations = [String]()
    var operationsDates = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = accountTitle

        //self.operations.removeAll()
        //self.operationsDates.removeAll()
        
        for operation in accountOperations
        {
            // DATE
            let timeValueAsNSNumber = operation["date"] as! NSNumber
            let timeValue = timeValueAsNSNumber.doubleValue/1000.0
            let opeDate = NSDate(timeIntervalSince1970: timeValue)
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateStyle = .ShortStyle
            dateFormatter.timeStyle = .NoStyle
            let opeDateFormat = dateFormatter.stringFromDate(opeDate)
            self.operationsDates.append(opeDateFormat)
            
            // LABEL
            let opeLabel = operation["label"] as! NSString
            
            // AMOUNT
            let opeAmount = operation["amount"] as! NSNumber
            let currencyFormatter = NSNumberFormatter()
            currencyFormatter.numberStyle = .CurrencyStyle
            currencyFormatter.stringFromNumber(opeAmount) // "123,44 €"
            let opeAmountFormat = currencyFormatter.stringFromNumber(opeAmount)!
            self.operations.append("\(opeLabel as String) = \(opeAmountFormat)")
            
            
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return operations.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("operationCell", forIndexPath: indexPath) 
        
        cell.textLabel!.text = operationsDates[indexPath.row]
        cell.textLabel?.textColor = accountColor
        cell.detailTextLabel!.text = operations[indexPath.row]
        return cell

        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
