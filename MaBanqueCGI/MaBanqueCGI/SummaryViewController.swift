//
//  SummaryViewController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 06/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import UIKit
import WatchKit
import WatchConnectivity

class SummaryViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITableViewDataSource, UITableViewDelegate, WCSessionDelegate {

    var login = String()

    var accountDesc = String()
    var accountColor = UIColor()
    var accountOperations = NSArray()
    
    var jsonClient = NSDictionary()
    
    let messageTitle = "✉️ Pour vous :"
    var opportunities = NSString()
    
    var cellColor = UIColor()
    var accounts = NSMutableArray()
    
    let gradientLayer = CAGradientLayer()
    let buttonMessageLayer = CAGradientLayer()
    let buttonSideBySideLayer = CAGradientLayer()


    
    @IBOutlet var accountsTableView: UITableView!
    
    @IBOutlet var sideBySideButton: UIButton!
    
    @IBOutlet var messageButton: UIButton!
    
    @IBOutlet var avatarButton: UIButton!
    
    @IBOutlet var background: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print("Summary login : " + login)
        
        // Masquer le bouton "Côte à côte"
        //sideBySideButton.hidden = true
        sideBySideButton.layer.cornerRadius = 10.0
        buttonSideBySideLayer.frame = self.sideBySideButton.bounds
        buttonSideBySideLayer.colors = [colorsCGI.rougeCGI.CGColor, colorsCGI.betteraveCGI.CGColor]
        buttonSideBySideLayer.locations = [0.50, 1.0]
        buttonSideBySideLayer.cornerRadius = 10.0
        sideBySideButton.layer.insertSublayer(buttonSideBySideLayer, atIndex: 0)
        
        

        // Gradient color background
        //http://blog.apoorvmote.com/gradient-background-uiview-ios-9-swift/?lang=fr
        gradientLayer.frame = self.view.bounds
        let color1 = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0).CGColor as CGColorRef
        let color2 = colorsCGI.nuageCGI.CGColor
        gradientLayer.colors = [color1, color2]
        gradientLayer.locations = [0.50, 1.0]
        self.view.layer.insertSublayer(gradientLayer, atIndex: 0)


        // Add both effects to your view
        //background.addMotionEffect(group)
        MotionFX.sharedInstance.setMotionFX(-30, max: 30)
        background.addMotionEffect(MotionFX.sharedInstance.getMotionFX())

        
        avatarButton.layer.borderColor = colorsCGI.nuageCGI.CGColor
        avatarButton.layer.cornerRadius = 2.0
        
        
        self.avatarButton.setImage(AvatarImage.sharedInstance.getImage(), forState: UIControlState.Normal)
        
        
        // Message button
        messageButton.hidden = true
        messageButton.layer.cornerRadius = 33.0
        buttonMessageLayer.frame = self.messageButton.bounds
        buttonMessageLayer.colors = [colorsCGI.rougeCGI.CGColor, colorsCGI.betteraveCGI.CGColor]
        buttonMessageLayer.locations = [0.50, 1.0]
        buttonMessageLayer.cornerRadius = 33.0
        messageButton.layer.insertSublayer(buttonMessageLayer, atIndex: 0)
        
        accountsTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        accountsTableView.backgroundColor = UIColor.clearColor()
        accountsTableView.layer.borderWidth = 2.0
        accountsTableView.layer.borderColor = colorsCGI.nuageCGI.CGColor
        
        self.navigationItem.backBarButtonItem?.title = "👤"
        
        //  WATCH : WCSessionDelegate
        if (WCSession.isSupported()) {
            let watchSession = WCSession.defaultSession()
            watchSession.delegate = self
            watchSession.activateSession()
        }
        
        
        
        //REST GET client
        //getJsonClient()
        
        //Get opportunities
        updateMessage()
        
        //REST GET POST accounts
        getJsonAccounts()
        
    }

    
    func getJsonAccounts(){
    
        //  Liste des comptes
        //  https://fscgi.com/fwk/rest/accounts/[IDENTIFIANT_CLIENT]
        //  ex : https://fscgi.com/fwk/rest/accounts/gagnonp
        
        //REST GET
        // URL as String
        let postEndPoint = "\(webSite.accounts)\(login)"
        let url = NSURL(string: postEndPoint)!
        
        let urlSession = NSURLSession.sharedSession()
        
        
        // Make the POST call and handle it in a completion handler
        urlSession.dataTaskWithURL(url, completionHandler: { ( data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // Make sure we get an OK response
            guard let realResponse = response as? NSHTTPURLResponse where
                realResponse.statusCode == 200 else {
                    print("Not a 200 response")
                    return
            }
            
            // Read the JSON
            do {
                if let accountsString = NSString(data:data!, encoding: NSUTF8StringEncoding) {
                    // Print what we got from the call
                    print("accountsString : \(accountsString)")
                    
                    //phoneDataExchange.jsonAccountsString = accountsString as String
                    phoneDataExchange.jsonAccountsData = data!
                    
                    //print(phoneDataExchange.jsonAccountsString)
                    
                    // Parse the JSON to get the Accounts detail
                    phoneDataExchange.jsonAccounts = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSMutableArray
                    
                    print("jsonAccounts => \(phoneDataExchange.jsonAccounts)")
                    
                    if phoneDataExchange.jsonAccounts.count != 0
                    {
                        
                        WatchData.shared.updateWatch("connection", dataObject: true )
                        
                        WatchData.shared.updateWatch("jsonAccountsData", dataObject:  phoneDataExchange.jsonAccountsData)
                        //self.updateWatchData(phoneDataExchange.jsonAccountsData, dataMessageLabel: "jsonAccountsData")
                        
                        WatchData.shared.updateWatch("avatarImageData", dataObject: phoneDataExchange.avatarImageData)
                                                
                        for account in phoneDataExchange.jsonAccounts {
                            self.accounts.addObject(self.labelFormatter(account as! NSDictionary) as String)
                        }
                        
                        // Update the label
                        self.performSelectorOnMainThread("updateAccountsLabel:", withObject: self.login, waitUntilDone: false)
                        
                        //Stop Spinner
                        (UIApplication.sharedApplication().delegate as! AppDelegate).setNetworkActivityIndicatorVisible(false)
                    
                    }
                }
            } catch {
                print("bad things happened")
            }
        }).resume()
        
    }

    /*
    func updateWatch(dataMessageLabel: String, dataObject: AnyObject) {
        
        //  WATCH
        // check the reachability
        if WCSession.defaultSession().reachable == false {
            print(" WATCH : WCSession unreachable")
            return
        }
        else{
            print(" WATCH : WCSession reachable")
            
            
            //phoneDataExchange.opportunities = self.opportunities as String
            
            WatchData.shared.sendDataToWatch(dataMessageLabel, data: dataObject)

            //WatchData.shared.sendDataToWatch(phoneDataExchange.opportunities, jsonAccountsString: phoneDataExchange.jsonAccountsString)
            
        }
    }
    */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sideBySideAction(sender: AnyObject) {
        // Ajout de la connexion "Côte à côte"
    
    }
 
    
    @IBAction func showMenu(sender: AnyObject) {
    
        //print("Bouton 'menu' appuyé")
        performSegueWithIdentifier("popoverMenu", sender: sender)
    }
    
    @IBAction func showMessage(sender: AnyObject) {

        let alertController = UIAlertController(title: messageTitle, message:
            self.opportunities as String, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "popoverMenu" {

            let popoverViewController = segue.destinationViewController
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
            popoverViewController.popoverPresentationController!.delegate = self
        }
        
        if segue.identifier == "pushToAccount" {

            let destinationVC = segue.destinationViewController as! AccountTableViewController
            destinationVC.accountTitle = accountDesc
            destinationVC.accountColor = accountColor
            destinationVC.accountOperations = accountOperations
        }
    
    }
    
   
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    
    
    func getJSON(urlToRequest: String) -> NSData{
        return NSData(contentsOfURL: NSURL(string: urlToRequest)!)!
    }
    

    
    //MARK: - Methods to update the UI immediately
    func updateAccountsLabel(text: String) {
        
        self.accountsTableView.reloadData()

    }
    
    func updatePostLabel(text: String) {
        //self.postResultLabel.text = "POST : " + text
        print("POST : \(text)")
    }
    

    func updateMessage(){
        if phoneDataExchange.opportunitiesArray.count == 0 {
            self.opportunities = ""
            self.messageButton.hidden = true
        }
        else {
            self.opportunities = phoneDataExchange.opportunities
            self.messageButton.hidden = false
        }
        
        //WatchData.shared.updateWatch("opportunities", dataObject: phoneDataExchange.opportunities)
        
    }

    func labelFormatter (account : NSDictionary) -> NSString {
       // Label formatter for the account title and balance
        let accountLabel : String! = account["label"] as! String!
        let accountBalance : NSNumber! = account["balance"] as! NSNumber!
        let balanceFormat = AmountFormat.sharedInstance.currencyFormatter(accountBalance)
        
        return ("\(accountLabel) = \(balanceFormat)")
        
    }
    
    @IBAction func unwindToSummary(segue: UIStoryboardSegue) {
        //http://stackoverflow.com/questions/12561735/what-are-unwind-segues-for-and-how-do-you-use-them
        
        //print("unwindToSummary")
        
    }
    
    
    // MARK: TableView methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.accounts.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.accountsTableView.dequeueReusableCellWithIdentifier("accountCell", forIndexPath: indexPath) as! AccountTableViewCell
        
        cell.accountButton.setTitle(self.accounts.objectAtIndex(indexPath.row) as? String, forState: UIControlState.Normal)
        cell.accountButton.addTarget(self, action: "performSegueToAccount:", forControlEvents: .TouchUpInside)
        cell.accountButton.tag = indexPath.row
        
        var modulo6 : NSInteger = NSInteger()
        
        modulo6 = indexPath.row % 6
        
        switch modulo6 {
            case 0:cellColor = colorsCGI.citrouilleCGI
            case 1:cellColor = colorsCGI.betteraveCGI
            case 2:cellColor = colorsCGI.mielCGI
            case 3:cellColor = colorsCGI.glaceCGI
            case 4:cellColor = colorsCGI.nuageCGI
            case 5:cellColor = colorsCGI.rougeCGI
            default:cellColor = colorsCGI.rougeCGI
        }
        accountColor = cellColor
        
        cell.accountButton.setTitleColor(cellColor, forState: UIControlState.Normal)
        cell.accountButton.layer.borderColor = cellColor.CGColor
       
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("pushToAccount", sender: self)
    }
    
    @IBAction func performSegueToAccount(sender: UIButton) {
        accountDesc = (sender.currentTitle)!
        accountColor = sender.currentTitleColor

        let account = phoneDataExchange.jsonAccounts[sender.tag] as! NSDictionary
        accountOperations = account["operations"] as! NSArray
        performSegueWithIdentifier("pushToAccount", sender: self)
    }
    
}
