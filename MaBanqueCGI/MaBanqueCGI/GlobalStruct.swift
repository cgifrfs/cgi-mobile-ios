//
//  GlobalStruct.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 21/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import UIKit

struct colorsCGI {
    
    // Couleurs CGI
    // ============
    static let rougeCGI = UIColor.init(red: 227.0/255.0, green: 25.0/255.0, blue: 55.0/255.0, alpha: 1.0)
    static let betteraveCGI = UIColor.init(red: 153.0/255.0, green: 31.0/255.0, blue: 61.0/255.0, alpha: 1.0)
    static let citrouilleCGI = UIColor.init(red: 255.0/255.0, green: 106.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let mielCGI = UIColor.init(red: 242.0/255.0, green: 162.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let nuageCGI = UIColor.init(red: 165.0/255.0, green: 172.0/255.0, blue: 176.0/255.0, alpha: 1.0)
    static let glaceCGI = UIColor.init(red: 161.0/255.0, green: 196.0/255.0, blue: 208.0/255.0, alpha: 1.0)

}

struct webSite {
    static let avatars = "https://fscgi.com/resources/image?path=/images/avatars/"
    static let session = "https://fscgi.com/fwk/rest/session"
    static let accounts = "https://fscgi.com/fwk/rest/accounts/"
    static let weather = "/weather"
    static let client = "https://fscgi.com/fwk/rest/clients/"

}

struct phoneDataExchange {
    
    static var jsonAccountsData = NSData()
    static var jsonAccounts = NSMutableArray()
    static var jsonLoginData = NSData()
    static var logged = false

    static var userName = ""
    static var userCivility = ""
    static var opportunities = ""
    static var opportunitiesArray : NSArray = NSArray()
    // Bouchon
    static var weatherDictionary: NSDictionary = ["status":"good","capacity":1600.00,"balance":436.78]
    static var heartRate = ""
    static var stepCount = ""
    static var avatarImageData = NSData()
    static var about = ""
    
    //Last Position
    static var lastPosDesc = ""
    static var lastPosLat = Double()
    static var lastPosLong = Double()
    
    //Addresses
    static var addressesArray : NSArray = NSArray()
    static var addressName = ""
    static var addressDesc = ""
    static var addressLat = Double()
    static var addressLong = Double()
    
}


class MotionFX {
    
    static var sharedInstance: MotionFX = MotionFX()

    // Set vertical effect
    private var vertical = UIInterpolatingMotionEffect(keyPath: "center.y",
        type: .TiltAlongVerticalAxis)
    
    // Set horizontal effect
    private var horizontal = UIInterpolatingMotionEffect(keyPath: "center.x",
        type: .TiltAlongHorizontalAxis)
    
    // Create group to combine both
    private var group = UIMotionEffectGroup()
    
    func setMotionFX(min: Int, max: Int){
        self.vertical.minimumRelativeValue = min
        self.vertical.maximumRelativeValue = max
        self.horizontal.minimumRelativeValue = min
        self.horizontal.maximumRelativeValue = max
    }
    
    func getMotionFX() -> UIMotionEffectGroup {
        self.group.motionEffects = [self.horizontal, self.vertical]
        return group
    }
    
}


// Singleton
class AvatarImage {
    
    static var sharedInstance: AvatarImage = AvatarImage()
    
    private var avatarImage = UIImage()
    
    func setImage(image: UIImage) {
        avatarImage = image
    }
    
    func getImage() -> UIImage {
        return avatarImage
    }
    
}

class AmountFormat {

    static var sharedInstance: AmountFormat = AmountFormat()
    
    func currencyFormatter (amount: NSNumber) -> NSString{
    // AMOUNT
    let currencyFormatter = NSNumberFormatter()
    currencyFormatter.numberStyle = .CurrencyStyle
    currencyFormatter.stringFromNumber(amount) // "123,44 €"
    let opeAmountFormat = currencyFormatter.stringFromNumber(amount)!
    return opeAmountFormat

    }
    
}













