//
//  QuestionnaireViewController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 16/02/2016.
//  Copyright © 2016 CGI-FS. All rights reserved.
//

import UIKit

class QuestionnaireViewController: UIViewController {

    @IBOutlet var background: UIImageView!

    @IBOutlet var submitButton: UIButton!
    //let gradientLayer = CAGradientLayer()
    let buttonLayer = CAGradientLayer()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        submitButton.layer.cornerRadius = 10.0
        buttonLayer.frame = self.submitButton.bounds
        buttonLayer.colors = [colorsCGI.rougeCGI.CGColor, colorsCGI.betteraveCGI.CGColor]
        buttonLayer.locations = [0.50, 1.0]
        buttonLayer.cornerRadius = 10.0
        submitButton.layer.insertSublayer(buttonLayer, atIndex: 0)
        
        /*
        // Gradient color background
        //http://blog.apoorvmote.com/gradient-background-uiview-ios-9-swift/?lang=fr
        gradientLayer.frame = self.view.bounds
        let color1 = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0).CGColor as CGColorRef
        let color2 = colorsCGI.nuageCGI.CGColor
        gradientLayer.colors = [color1, color2]
        gradientLayer.locations = [0.50, 1.0]
        self.view.layer.insertSublayer(gradientLayer, atIndex: 0)
        */
        
        background.addMotionEffect(MotionFX.sharedInstance.getMotionFX())

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
