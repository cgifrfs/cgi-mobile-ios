//
//  WatchGlobalStruct.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 27/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import Foundation
import UIKit

struct colorsCGI {
    
    // Couleurs CGI
    // ============
    static let rougeCGI = UIColor.init(red: 227.0/255.0, green: 25.0/255.0, blue: 55.0/255.0, alpha: 1.0)
    static let betteraveCGI = UIColor.init(red: 153.0/255.0, green: 31.0/255.0, blue: 61.0/255.0, alpha: 1.0)
    static let citrouilleCGI = UIColor.init(red: 255.0/255.0, green: 106.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let mielCGI = UIColor.init(red: 242.0/255.0, green: 162.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let nuageCGI = UIColor.init(red: 165.0/255.0, green: 172.0/255.0, blue: 176.0/255.0, alpha: 1.0)
    static let glaceCGI = UIColor.init(red: 161.0/255.0, green: 196.0/255.0, blue: 208.0/255.0, alpha: 1.0)
    
}

struct watchDataExchange {
    
    static var connection = false
    
    static var jsonLoginData = NSData()
    static var jsonAccounts = NSArray()
    static var jsonAccountsData = NSData()
    
    //User
    static var userName = ""
    static var userCivility = ""
    static var opportunities = ""
    static var opportunitiesArray : NSArray = NSArray()
    
    //Last Position
    static var lastPosDesc = ""
    static var lastPosLat = Double()
    static var lastPosLong = Double()
    
    //Addresses
    static var addressesArray : NSArray = NSArray()
    static var addressName = ""
    static var addressDesc = ""
    static var addressLat = Double()
    static var addressLong = Double()

    
    
    
    //Accounts
    static var accountsLabels = NSMutableArray()
    static var accountsBalances = NSMutableArray()
    static var accountsOperations = NSMutableArray()
    static var accountsColors = NSMutableArray()

    //Selected account
    static var selectedAccountOperations = NSMutableArray()
    static var selectedAccountLabel = String()
    static var selectedAccountColor: UIColor = UIColor.whiteColor()
    static var selectedAccountBalance = ""


    //Operations
    static var operations = [String]()
    static var operationsDates = [String]()
    
    //Health
    static var heartRate = ""
    static var stepCount = ""
    
    static var avatarImageData = NSData()

    //À propos
    static var about0 = "Ma Banque CGI"
    static var about1 = "Crédit : © 2016 \nGroupe CGI inc."
    static var about2 = "Entité : \nFinancial Services"
    static var about3 = "Projet : \nInnovation 2016"
    static var about4 = "Version : \n1.0 Beta"
    static var about5 = "Date de màj : \nFévrier 2016"
    static var about6 = "Responsable projet : \nAlban NOGUÈS"
    static var about7 = "Supervision : \nUgo PARLANGE"
    static var about8 = "Développement : \nGhislain FORTIN"
    static var about = "\(about0)\n\n\(about1)\n\n\(about2)\n\n\(about3)\n\n\(about4)\n\n\(about5)\n\n\(about6)\n\n\(about7)\n\n\(about8)"
    
    //Bouchons
    static var weatherDictionary: NSDictionary = ["status":"good","capacity":1600.00,"balance":436.78]
    // Weather
    static var weatherStatus: String = watchDataExchange.weatherDictionary["status"] as! String
    static var weatherCapacity = watchDataExchange.weatherDictionary["capacity"] as! Int
    static var weatherBalanceString = "Solde : \(String(watchDataExchange.weatherDictionary["balance"]!)) €"
    static var weatherBalanceFloat = watchDataExchange.weatherDictionary["balance"] as! Float
    static var weatherEmoji = ""


    

    
}
