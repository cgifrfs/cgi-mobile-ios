//
//  ComplicationController.swift
//  temp Extension
//
//  Created by Ghislain Fortin on 04/02/2016.
//  Copyright © 2016 CGI-FS. All rights reserved.
//

import ClockKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    
    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirectionsForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.Forward, .Backward])
    }
    
    func getTimelineStartDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(nil)
    }
    
    func getPrivacyBehaviorForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.ShowOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntryForComplication(complication: CLKComplication, withHandler handler: ((CLKComplicationTimelineEntry?) -> Void)) {
        // Call the handler with the current timeline entry
        handler(nil)
    }
    
    func getTimelineEntriesForComplication(complication: CLKComplication, beforeDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        // Call the handler with the timeline entries prior to the given date
        handler(nil)
    }
    
    func getTimelineEntriesForComplication(complication: CLKComplication, afterDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        // Call the handler with the timeline entries after to the given date
        handler(nil)
    }
    
    // MARK: - Update Scheduling
    
    func getNextRequestedUpdateDateWithHandler(handler: (NSDate?) -> Void) {
        // Call the handler with the date when you would next like to be given the opportunity to update your complication content
        handler(nil);
    }
    
    // MARK: - Placeholder Templates
    
    func getPlaceholderTemplateForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTemplate?) -> Void) {
        // This method will be called once per supported complication, and the results will be cached
        //handler(nil)
        
        var template: CLKComplicationTemplate?
        
        switch complication.family {
        case .ModularSmall:
            /*
            let modularSmallTemplate =
            CLKComplicationTemplateModularSmallRingText()
            
            modularSmallTemplate.textProvider =
                CLKSimpleTextProvider(text: "R")
            modularSmallTemplate.fillFraction = 0.75
            modularSmallTemplate.ringStyle = CLKComplicationRingStyle.Closed
            template = modularSmallTemplate
            */
            template = nil
        case .ModularLarge:
            let modularLargeTemplate =
            CLKComplicationTemplateModularLargeStandardBody()
            
            if (watchDataExchange.opportunities == "") {
                modularLargeTemplate.headerTextProvider =
                    CLKSimpleTextProvider(text: "Ma Banque CGI", shortText: "Banque CGI")
                modularLargeTemplate.body1TextProvider =
                    CLKSimpleTextProvider(text: "", shortText: "")
                modularLargeTemplate.body2TextProvider =
                    CLKSimpleTextProvider(text: "", shortText: "")
            } else {
                modularLargeTemplate.headerTextProvider =
                    CLKSimpleTextProvider(text: "Ma Banque CGI", shortText: "Banque CGI")
                modularLargeTemplate.body1TextProvider =
                    CLKSimpleTextProvider(text: "Pour vous :", shortText: "Pour vous :")
                modularLargeTemplate.body2TextProvider =
                    CLKSimpleTextProvider(text: watchDataExchange.opportunities, shortText: watchDataExchange.opportunities)
            }
            template = modularLargeTemplate
        case .UtilitarianSmall:
            template = nil
        case .UtilitarianLarge:
            let utilitarianLargeTemplate = CLKComplicationTemplateUtilitarianLargeFlat()
            //utilitarianLargeTemplate.imageProvider = CLKImageProvider(onePieceImage: UIImage(imageLiteral: "watch-inno-200.png"))
            utilitarianLargeTemplate.textProvider = CLKSimpleTextProvider(text: watchDataExchange.opportunities, shortText: watchDataExchange.opportunities)
            template = utilitarianLargeTemplate
            
        case .CircularSmall:
            template = nil
        }
        
        handler(template)
        
        
    }
    
}
