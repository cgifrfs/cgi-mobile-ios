//
//  WatchInterfaceController.swift
//  MaBanqueCGI-watch Extension
//
//  Created by Ghislain Fortin on 09/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import WatchKit
import UIKit
//import WatchConnectivity
import Foundation
//import HealthKit


class WatchInterfaceController: WKInterfaceController {
    
    @IBOutlet var opportunities: WKInterfaceButton!
    @IBOutlet var balanceGauge: WKInterfaceSlider!
    @IBOutlet var balanceAmount: WKInterfaceLabel!
    @IBOutlet var accountsTable: WKInterfaceTable!
    @IBOutlet var heartEmoji: WKInterfaceLabel!
    @IBOutlet var heartRate: WKInterfaceLabel!
    @IBOutlet var stepEmoji: WKInterfaceLabel!
    @IBOutlet var stepCount: WKInterfaceLabel!
    @IBOutlet var weatherEmojiButton: WKInterfaceButton!

    @IBOutlet var loginGroup: WKInterfaceGroup!
    @IBOutlet var logoutGroup: WKInterfaceGroup!
    @IBOutlet var oppWeatherGroup: WKInterfaceGroup!
    @IBOutlet var heartGroup: WKInterfaceGroup!
    @IBOutlet var stepGroup: WKInterfaceGroup!
    @IBOutlet var balanceGroup: WKInterfaceGroup!
    
    @IBOutlet var avatarGroup: WKInterfaceGroup!
    @IBOutlet var summaryLabel: WKInterfaceLabel!
    @IBOutlet var avatarImage: WKInterfaceImage!
    
    var accountColor: UIColor = UIColor()
    
    //HealthStore
    
    /*
    let healthStore: HKHealthStore? = {
        if HKHealthStore.isHealthDataAvailable() {
            print(HKHealthStore())
            return HKHealthStore()
        } else {
            print("HKHealthStore == nil")
            return nil
        }
    }()
    */
    
    @IBAction func opportunitiesAction() {
        
        print("Opportunities action")
        
        self.showMessage("✉️ Pour vous :", msg: watchDataExchange.opportunities)
        
    }
    
    @IBAction func menuMap() {
        print("Plan")

        pushControllerWithName("mapScene",context: ["segue": "hierarchical","data":"Passed through hierarchical navigation"])
    }
    
    @IBAction func menuAbout() {
        print("À propos")
        //self.showMessage("⌚️ À propos :", msg: watchDataExchange.about)
        pushControllerWithName("aboutScene",context: ["segue": "hierarchical","data":"Passed through hierarchical navigation"])
        //pushControllerWithName("AboutScene",context: ["segue": "pagebased","data":"Passed through pagebased navigation"])
        
    }
    

    @IBAction func weatherAction() {
        print("Weather")
        
        self.showMessage("Météo du compte", msg: "\(watchDataExchange.weatherEmoji)\n\(watchDataExchange.weatherBalanceString)")
                
    }
    
    @IBAction func menuQuit() {
        print("Quitter")
        if watchDataExchange.connection == true
        {
            watchDataExchange.connection = false
            popToRootController()
            showOrHide()
        }
        else
        {
            self.showMessage("Connexion", msg: "Veuillez vous identifier à l'aide de votre iPhone.")
        }
    }
    
    
    @IBAction func buttonAbout() {
        print("bouton A propos")
        self.showMessage("⌚️ À propos :", msg:watchDataExchange.about)
    }
    

    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        
        // Initialize the 'WCSession'.
        WatchData.shared.activate()
        interfaceDelegate = self

        
        // Configure interface objects here.
        
        /*
        //HealthKit
        if HKHealthStore.isHealthDataAvailable() {
            print("HealthStore is available")

            stepCounter()
            getHeartRate()
        }
        else {
            print("HealthStore is not available")
        }
        */
        
        summaryLabel.setTextColor(colorsCGI.rougeCGI)
        
        // Weather
        /*
        let status: String = watchDataExchange.weatherDictionary["status"] as! String
        let capacity = watchDataExchange.weatherDictionary["capacity"] as! Int
        let balanceString = "Solde : \(String(watchDataExchange.weatherDictionary["balance"]!)) €"
        let balanceFloat = watchDataExchange.weatherDictionary["balance"] as! Float
        */

        balanceGauge.setValue(watchDataExchange.weatherBalanceFloat * 100)
        balanceGauge.setNumberOfSteps(watchDataExchange.weatherCapacity * 100)
        balanceAmount.setText(watchDataExchange.weatherBalanceString)

        balanceGauge.setColor(colorsCGI.citrouilleCGI)
        
        
        if UIImage(data: watchDataExchange.avatarImageData) == nil {
            print("no image")
            print(watchDataExchange.avatarImageData)
        }
        else{
            print("image OK")
            let avatarImage = UIImage(data: watchDataExchange.avatarImageData)
            print(avatarImage)
            //self.avatarImage.setImage(avatarImage)
        }
        
        
        
        switch watchDataExchange.weatherStatus {
        case "good"  :
            balanceGauge.setColor(colorsCGI.mielCGI)
            watchDataExchange.weatherEmoji = "☀️"
        case "middling"  :
            balanceGauge.setColor(colorsCGI.citrouilleCGI)
            watchDataExchange.weatherEmoji = "🌤"
        case "bad"  :
            balanceGauge.setColor(colorsCGI.rougeCGI)
            watchDataExchange.weatherEmoji = "🌧"
        default :
            print( "Error getting weather")
            balanceGauge.setColor(colorsCGI.rougeCGI)
            watchDataExchange.weatherEmoji = "📵"
        }
        
        weatherEmojiButton.setTitle(watchDataExchange.weatherEmoji)

        updateHealthInfo()

        showOrHide()

        
    }
    
    func updateHealthInfo(){
        
        if watchDataExchange.heartRate == "" {
            heartRate.setText("")
        }
        else{
            heartRate.setText("\(watchDataExchange.heartRate) BPM")
        }
        
        if watchDataExchange.stepCount == "" {
            stepCount.setText("")
        }
        else
        {
            stepCount.setText("\(watchDataExchange.stepCount) pas")
        }
        
    }
    
    func updateAvatar(){
        
        if UIImage(data: watchDataExchange.avatarImageData) == nil {
            print("no image")
        }
        else{
            print("image OK")
            let avatarImage = UIImage(data: watchDataExchange.avatarImageData)
            print(avatarImage)
            self.avatarImage.setImage(avatarImage)
        }
        
    }
    
    func showOrHide() {
        
        //print("showOrHide")
                
        if watchDataExchange.connection == true {
            // Login
            loginGroup.setHidden(false)
            logoutGroup.setHidden(true)
            balanceGroup.setHidden(true)
            if watchDataExchange.opportunities == "" {
                opportunities.setHidden(true)
            }
            else {
                opportunities.setHidden(false)
            }
            //loadTableData()
        }
        else {
            // Logout
            loginGroup.setHidden(true)
            logoutGroup.setHidden(false)
            watchDataExchange.accountsLabels.removeAllObjects()
            watchDataExchange.accountsBalances.removeAllObjects()
            watchDataExchange.selectedAccountOperations.removeAllObjects()
            watchDataExchange.operationsDates.removeAll()
            watchDataExchange.operations.removeAll()
            if watchDataExchange.stepCount == ""
            {
                stepGroup.setHidden(true)
            }
            else
            {
                stepGroup.setHidden(false)
            }
            if watchDataExchange.heartRate == ""
            {
                heartGroup.setHidden(true)
            }
            else
            {
                heartGroup.setHidden(false)
            }
        }
        
        
        
    }
    
    
    func loadTableData() {

        accountsTable.setNumberOfRows(watchDataExchange.accountsLabels.count, withRowType: "AccountTableRowID")
        
        for (index, accountLabel) in watchDataExchange.accountsLabels.enumerate() {
            
            let row = accountsTable.rowControllerAtIndex(index) as! WatchAccountsTableRowController
            
            row.accountLabel.setText((accountLabel as! String))
            
            var modulo6 : NSInteger = NSInteger()
            
            modulo6 = index % 6
            
            switch modulo6 {
            case 0:accountColor = colorsCGI.citrouilleCGI
            case 1:accountColor = colorsCGI.glaceCGI
            case 2:accountColor = colorsCGI.mielCGI
            case 3:accountColor = colorsCGI.betteraveCGI
            case 4:accountColor = colorsCGI.nuageCGI
            case 5:accountColor = colorsCGI.rougeCGI
            default:accountColor = colorsCGI.rougeCGI
            }
            
            watchDataExchange.accountsColors[index] = accountColor
            
            row.accountLabel.setTextColor(accountColor)
            
        }
    }
    
/*
    private func getHeartRate(){
    
        let heartRateType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        
        if (HKHealthStore.isHealthDataAvailable()){
            
            var csvString = "Time,Date,Heartrate(BPM)\n"
            
            print("csvString 1 : \(csvString)")
            
            self.heartRate.setText(String(csvString))

            
            self.healthStore!.requestAuthorizationToShareTypes(nil, readTypes:[heartRateType], completion:{(success, error) in
                let sortByTime = NSSortDescriptor(key:HKSampleSortIdentifierEndDate, ascending:false)
                let timeFormatter = NSDateFormatter()
                timeFormatter.dateFormat = "hh:mm:ss"
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd/MM/YYYY"
                
                let query = HKSampleQuery(sampleType:heartRateType, predicate:nil, limit:1, sortDescriptors:[sortByTime], resultsHandler:{(query, results, error) in
                    guard let results = results else { return }
                    for quantitySample in results {
                        let quantity = (quantitySample as! HKQuantitySample).quantity
                        let heartRateUnit = HKUnit(fromString: "count/min")
                        

                        csvString += "\(timeFormatter.stringFromDate(quantitySample.startDate)),\(dateFormatter.stringFromDate(quantitySample.startDate)),\(quantity.doubleValueForUnit(heartRateUnit))\n"
                        
                        print("csvString 2 : \(csvString)")
                        
                        self.heartRate.setText(String(csvString))

                        
                        print("\(timeFormatter.stringFromDate(quantitySample.startDate)),\(dateFormatter.stringFromDate(quantitySample.startDate)),\(quantity.doubleValueForUnit(heartRateUnit))")
                    }
                    
                    do {
                        let documentsDir = try NSFileManager.defaultManager().URLForDirectory(.DocumentDirectory, inDomain:.UserDomainMask, appropriateForURL:nil, create:true)
                        try csvString.writeToURL(NSURL(string:"heartratedata.csv", relativeToURL:documentsDir)!, atomically:true, encoding:NSASCIIStringEncoding)
                    }
                    catch {
                        print("Error occured")
                    }
                    
                })
                
                print("healthStore query : \(query)")
                self.healthStore!.executeQuery(query)
            })
        }
    
    }
    
    
    func stepCounter() {
        //http://stackoverflow.com/questions/28802053/healthkit-step-counter
        
        let endDate = NSDate()
        let startDate = NSCalendar.currentCalendar().dateByAddingUnit(.Month, value: -1, toDate: endDate, options: .MatchFirst)
        
        let weightSampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)
        let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: .None)
        
        let query = HKSampleQuery(sampleType: weightSampleType!, predicate: predicate, limit: 0, sortDescriptors: nil, resultsHandler: {
            (query, results, error) in
            if results == nil {
                print("There was an error running the query: \(error)")
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                var dailyAVG:Double! = 0
                for steps in results as! ([HKQuantitySample])!
                {
                    // add values to dailyAVG
                    dailyAVG = dailyAVG + steps.quantity.doubleValueForUnit(HKUnit.countUnit())
                    print(dailyAVG)
                    print(steps)
                }
            }
        })
        
        self.healthStore?.executeQuery(query)
        
    }
 */


    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject?
    {
        let accountLabel = watchDataExchange.accountsLabels[rowIndex]

        watchDataExchange.selectedAccountBalance = watchDataExchange.accountsBalances[rowIndex] as! String
        watchDataExchange.selectedAccountLabel = watchDataExchange.accountsLabels[rowIndex] as! String
        watchDataExchange.selectedAccountColor = watchDataExchange.accountsColors[rowIndex] as! UIColor
        
        print("TEST SEGUE : \(watchDataExchange.accountsOperations[rowIndex])")
        watchDataExchange.selectedAccountOperations = watchDataExchange.accountsOperations[rowIndex] as! NSMutableArray
        print("TEST SEGUE 2 : \(watchDataExchange.selectedAccountOperations)")
        return accountLabel
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        NSLog("%@ will activate", self)
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        NSLog("%@ did deactivate", self)
        super.didDeactivate()
    }
    

    /*
    func session(watchSession: WCSession,
        didReceiveUserInfo opportunities: [String : AnyObject]){
            print("Session received called")
            print(opportunities)
    }
    */
    
    internal func showMessage(title:String, msg:String){
        let defaultAction = WKAlertAction(title: "OK", style: WKAlertActionStyle.Default) { () -> Void in }
        let actions = [defaultAction]
        self.presentAlertControllerWithTitle( title,  message: msg,  preferredStyle: WKAlertControllerStyle.Alert, actions: actions)
    }

    
    
}
