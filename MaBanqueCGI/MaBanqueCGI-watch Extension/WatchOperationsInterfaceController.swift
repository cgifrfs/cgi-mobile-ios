//
//  MessageInterfaceController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 27/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import WatchKit
import Foundation


class WatchOperationsInterfaceController: WKInterfaceController {

    @IBOutlet var accountLabel: WKInterfaceLabel!
    
    @IBOutlet var accountBalance: WKInterfaceLabel!
    
    @IBOutlet var operationsTable: WKInterfaceTable!
    
    @IBAction func menuSummary() {
        print("Synthèse")
        popController()
    }
    
    @IBAction func menuAbout() {
        pushControllerWithName("aboutScene",context: ["segue": "hierarchical","data":"Passed through hierarchical navigation"])
    }
    
    
    @IBAction func menuMap() {
        print("Plan")
        pushControllerWithName("mapScene",context: ["segue": "hierarchical","data":"Passed through hierarchical navigation"])
        
    }
    

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        
        accountLabel.setText(watchDataExchange.selectedAccountLabel)
        accountLabel.setTextColor(watchDataExchange.selectedAccountColor)
        accountBalance.setText(watchDataExchange.selectedAccountBalance)
        
        if watchDataExchange.selectedAccountOperations.count != 0 {
            for operation in watchDataExchange.selectedAccountOperations
            {
                // DATE
                let timeValueAsNSNumber = operation["date"] as! NSNumber
                let timeValue = timeValueAsNSNumber.doubleValue/1000.0
                let opeDate = NSDate(timeIntervalSince1970: timeValue)
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateStyle = .ShortStyle
                dateFormatter.timeStyle = .NoStyle
                let opeDateFormat = dateFormatter.stringFromDate(opeDate)
                watchDataExchange.operationsDates.append(opeDateFormat)
                
                // LABEL
                let opeLabel = operation["label"] as! NSString
                
                // AMOUNT
                let opeAmount = operation["amount"] as! NSNumber
                let currencyFormatter = NSNumberFormatter()
                currencyFormatter.numberStyle = .CurrencyStyle
                currencyFormatter.stringFromNumber(opeAmount) // "123,44 €"
                let opeAmountFormat = currencyFormatter.stringFromNumber(opeAmount)!
                watchDataExchange.operations.append("\(opeLabel as String) = \(opeAmountFormat)")
            }
        }

        
        if watchDataExchange.operations.count != 0 {
            operationsTable.setNumberOfRows(watchDataExchange.operations.count, withRowType: "OperationTableRowID")
            
            for (index, _) in watchDataExchange.operations.enumerate() {
                
                let row = operationsTable.rowControllerAtIndex(index) as! WatchOperationsTableRowController
                
                row.operationLabel.setText(watchDataExchange.operations[index])
                row.operationDate.setText(watchDataExchange.operationsDates[index])
                row.operationDate.setTextColor(watchDataExchange.selectedAccountColor)
            }
        }
        else
        {
            operationsTable.setNumberOfRows(0, withRowType: "OperationTableRowID")
        }
    }


    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        watchDataExchange.operations.removeAll()
        watchDataExchange.operationsDates.removeAll()

        super.didDeactivate()
        
    }

}
