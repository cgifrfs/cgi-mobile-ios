//
//  GlanceInterfaceController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 03/02/2016.
//  Copyright © 2016 CGI-FS. All rights reserved.
//

import WatchKit
import Foundation


class GlanceInterfaceController: WKInterfaceController {

    @IBOutlet var balanceAmount: WKInterfaceLabel!
    
    @IBOutlet var lastUpdate: WKInterfaceLabel!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        
        var Timestamp: String {
            return "\(NSDate().timeIntervalSince1970 * 1000)"
        }
        
        balanceAmount.setText("\n\(watchDataExchange.accountsLabels[0] as! String) \n \(watchDataExchange.accountsBalances[0] as! String)")
        
        
        // Update timestamp
        let update = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .ShortStyle
        dateFormatter.timeStyle = .ShortStyle
        let updateFormat = dateFormatter.stringFromDate(update)
        
        //lastUpdate.setText("Màj : \(updateFormat)")
        lastUpdate.setText("Màj : \(updateFormat)")

        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
