//
//  WatchAboutInterfaceController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 14/12/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import WatchKit
import Foundation


class WatchAboutInterfaceController: WKInterfaceController {

    @IBOutlet var labelAbout: WKInterfaceLabel!
    @IBOutlet var playVideo: WKInterfaceMovie!
   
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
            
        let url = NSBundle.mainBundle().URLForResource("movieclip", withExtension: "mov")
            
        playVideo.setMovieURL(url!)
        
        
                
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        
        labelAbout.setText(watchDataExchange.about)

        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    
}
