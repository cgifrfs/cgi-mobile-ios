//
//  WatchOperationsTableRowController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 28/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import WatchKit

class WatchOperationsTableRowController: NSObject {

    @IBOutlet var operationDate: WKInterfaceLabel!

    @IBOutlet var operationLabel: WKInterfaceLabel!

}