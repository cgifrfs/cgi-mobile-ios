//
//  WatchData.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 27/10/2015.
//  Copyright © 2015 CGI-FS. All rights reserved.
//

import Foundation
import WatchConnectivity
import WatchKit

//http://stackoverflow.com/questions/27025526/passing-data-to-apple-watch-app

@available(iOS 9.0, *)
var interfaceDelegate:WatchInterfaceController? = nil

public class WatchData: NSObject,WCSessionDelegate {
    var session = WCSession.defaultSession()
    //
    
    class var shared: WatchData {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: WatchData? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = WatchData()
        }
        return Static.instance!
    }
    
    public func session(session: WCSession, didReceiveFile file: WCSessionFile){
        print(__FUNCTION__)
        print(session)
        
    }
    
    
    public func session(session: WCSession, didReceiveApplicationContext applicationContext: [String : AnyObject]) {
        print(__FUNCTION__)
        print(session)
        
        //interfaceDelegate?.showMessage("didReceiveApplicationContext")
    }
    
    
    public func sessionReachabilityDidChange(session: WCSession){
        print(__FUNCTION__)
        print(session)
        print("reachability changed:\(session.reachable)")
        /*
        let text = session.reachable ? "reachable" : "unreachable"
        interfaceDelegate?.showMessage(text)
        */
    }
    
    public func sessionWatchStateDidChange(session: WCSession) {
        print(__FUNCTION__)
        print(session)
        print("reachable:\(session.reachable)")
        
        /*
         interfaceDelegate?.showMessage("sessionWatchStateDidChange")
        if !session.receivedApplicationContext.keys.isEmpty {
            interfaceDelegate?.showMessage(session.receivedApplicationContext.description)
        }
        */
    }
    
    public func session(session: WCSession, didReceiveMessageData messageData: NSData){
        print(__FUNCTION__)

        if !session.receivedApplicationContext.keys.isEmpty {
            //interfaceDelegate?.showMessage(session.receivedApplicationContext.description)
        }
    }
    
    
    public func session(session: WCSession, didReceiveMessage message: [String : AnyObject]){
        print(__FUNCTION__)
        
        
        if let data = message["data"] {
            print(data)
            //interfaceDelegate?.showMessage(data as! String)
            return
        }
        
        
    }
    
    public func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        print(__FUNCTION__)
        
        if let data = message["heartRate"] {
            
            watchDataExchange.heartRate = data as! String
            
            print("watchDataExchange.heartRate : \(watchDataExchange.heartRate) BPM")
            
            interfaceDelegate?.updateHealthInfo()
            interfaceDelegate?.showOrHide()

            
            return
        }
        
        if let data = message["stepCount"] {
            
            watchDataExchange.stepCount = data as! String
            
            print("watchDataExchange.stepCount : \(watchDataExchange.stepCount) pas ce mois")
            
            interfaceDelegate?.updateHealthInfo()
            interfaceDelegate?.showOrHide()

            
            return
        }
        
        /*
        if let data = message["about"] {
            
            watchDataExchange.about = data as! String
            
            print("watchDataExchange.about : \(watchDataExchange.about)")
   
            return
        }
        */
        
        
        if let data = message["avatarImageData"] {
            
            watchDataExchange.avatarImageData = data as! NSData
            
            print("watchDataExchange.avatarImageData : OK")
            
            interfaceDelegate?.updateAvatar()
            interfaceDelegate?.showOrHide()
            
            return
        }
        
        
        if let data = message["connection"] {
            
            watchDataExchange.connection = data as! Bool
            
            print("watchDataExchange.connection : \(watchDataExchange.connection)")
            
            
            if watchDataExchange.connection == false {
                interfaceDelegate?.showOrHide()
            }
                
            return
        }

        /*
        if let data = message["opportunities"] {

            watchDataExchange.opportunities = data as! String
            
            /*
            if watchDataExchange.opportunities != "" {
            interfaceDelegate?.showMessage(watchDataExchange.opportunities)
            }
            */
            
            print("watchDataExchange.opportunities : \(watchDataExchange.opportunities)")
            
            //interfaceDelegate?.showOrHide()
            
            return
        }
        */
        
        if let data = message["jsonLoginData"] {
            
            watchDataExchange.jsonLoginData = data as! NSData

            print("watchDataExchange.jsonLoginData : \(watchDataExchange.jsonLoginData)")
            
            do {
                if let loginString = NSString(data:data as! NSData, encoding: NSUTF8StringEncoding) {
                    // Print what we got from the call
                    print("loginString :  \(loginString)")
                    
                    // Parse the JSON to get the Login detail
                    let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data as! NSData, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    
                    let userCivility: NSDictionary! = jsonDictionary["civility"]! as! NSDictionary
                    let lastPosition: NSDictionary! = jsonDictionary["lastPosition"]! as! NSDictionary

                    
                    watchDataExchange.userName = jsonDictionary["lastName"]! as! String
                    watchDataExchange.userCivility = userCivility["label"]! as! String
                    
                    if lastPosition.count == 0
                    {
                        watchDataExchange.lastPosDesc = ""
                        watchDataExchange.lastPosLat = 0.000
                        watchDataExchange.lastPosLong = 0.000
                    }
                    else
                    {
                        if lastPosition["description"] is NSNull
                        {
                            watchDataExchange.lastPosDesc = ""
                        }
                        else
                        {
                            watchDataExchange.lastPosDesc = lastPosition["description"]! as! String
                        }
                        watchDataExchange.lastPosLat = lastPosition["latitude"]! as! Double
                        watchDataExchange.lastPosLong = lastPosition["longitude"]! as! Double
                    }
                    
                    watchDataExchange.addressesArray = jsonDictionary["addresses"]! as! NSArray
                    
                    
                    if watchDataExchange.addressesArray.count == 0
                    {
                        watchDataExchange.addressName = ""
                        watchDataExchange.addressDesc = ""
                        watchDataExchange.addressLat = 0.000
                        watchDataExchange.addressLong = 0.000

                    }
                    else
                    {
                        let address: NSDictionary = watchDataExchange.addressesArray[0] as! NSDictionary
                        watchDataExchange.addressName = address["name"]! as! String
                        watchDataExchange.addressDesc = address["description"]! as! String
                        watchDataExchange.addressLat = address["latitude"]! as! Double
                        watchDataExchange.addressLong = address["longitude"]! as! Double
                    }
                    
                    
                    watchDataExchange.opportunitiesArray = jsonDictionary["opportunities"]! as! NSArray
 
                    
                    if watchDataExchange.opportunitiesArray.count == 0
                    {
                        watchDataExchange.opportunities = ""
                    }
                    else
                    {
                        let oppName: NSDictionary = watchDataExchange.opportunitiesArray[0] as! NSDictionary
                        watchDataExchange.opportunities = oppName["name"]! as! String

                    }
                    
                }
            } catch {
                print("bad things happened")
            }
            
            return
        }
        
        
        if let data = message["jsonAccountsData"] {
            
            watchDataExchange.jsonAccountsData = data as! NSData
            
            /*
            if watchDataExchange.jsonAccounts != "" {
                interfaceDelegate?.showMessage(watchDataExchange.jsonAccounts)
            }
            */
            
            //print("watchDataExchange.jsonAccountsData : \(watchDataExchange.jsonAccountsData)")
            
            // ==================
            // Read the JSON
            do {
                if let accountsString = NSString(data:data as! NSData, encoding: NSUTF8StringEncoding) {
                    
                    print(accountsString)
                    
                    // Parse the JSON to get the Accounts detail
                    watchDataExchange.jsonAccounts = try NSJSONSerialization.JSONObjectWithData(data as! NSData, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                    
                    print("jsonAccounts => \(watchDataExchange.jsonAccounts)")
                    
                    if watchDataExchange.jsonAccounts.count != 0
                    {
                        for account in watchDataExchange.jsonAccounts {
                            //print(account["label"]!)
                            
                            watchDataExchange.accountsLabels.addObject(account["label"] as! String)
                            watchDataExchange.accountsOperations.addObject(account["operations"] as! NSArray)

                            print(watchDataExchange.accountsLabels)
                            
                            let accountBalance : NSNumber! = account["balance"] as! NSNumber!
                            let balanceFormat = self.currencyFormatter(accountBalance)
                            
                            print(balanceFormat)
                            
                            watchDataExchange.accountsBalances.addObject(balanceFormat)

                            //self.accounts.addObject(self.labelFormatter(account as! NSDictionary) as String)
                            
                        }
                        
                        // Update the label
                        //self.performSelectorOnMainThread("updateAccountsLabel:", withObject: self.login, waitUntilDone: false)
                    }
                }
            } catch {
                print("bad things happened")
            }

        //==============
            
            interfaceDelegate?.loadTableData()
            return
        }
        
        //guard message["request"] as? String == "showAlert" else {return}
        
    }
    
    func currencyFormatter (amount: NSNumber) -> NSString{
        // AMOUNT
        let currencyFormatter = NSNumberFormatter()
        currencyFormatter.numberStyle = .CurrencyStyle
        currencyFormatter.stringFromNumber(amount) // "123,44 €"
        let opeAmountFormat = currencyFormatter.stringFromNumber(amount)!
        return opeAmountFormat
    }
    
    
    public func activate(){
        
        if WCSession.isSupported() {    //  it is supported
            session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
            print("watch activating WCSession")
        } else {
            
            print("watch does not support WCSession")
        }
        
        if(!session.reachable){
            print("not reachable")
            return
        }else{
            print("watch is reachable")
            
        }
    }

}