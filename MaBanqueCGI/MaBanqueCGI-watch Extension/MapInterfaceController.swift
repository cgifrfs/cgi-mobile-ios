//
//  MapInterfaceController.swift
//  MaBanqueCGI
//
//  Created by Ghislain Fortin on 09/02/2016.
//  Copyright © 2016 CGI-FS. All rights reserved.
//

import WatchKit
import Foundation


class MapInterfaceController: WKInterfaceController {

    @IBOutlet var mapObject: WKInterfaceMap!
    
    var mapLocation: CLLocationCoordinate2D?
    

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    
        //let parentValues = ["task" : "geoLocation"]
        
        
        let span = MKCoordinateSpanMake(0.1, 0.1)
        
        if watchDataExchange.lastPosLat == 0
        {
            print("No last position recorded")
        }
        else
        {
            // Add annotation to Last position
        
            mapLocation = CLLocationCoordinate2DMake(watchDataExchange.lastPosLat,watchDataExchange.lastPosLong)
            let region = MKCoordinateRegionMake(mapLocation!,span)
            self.mapObject.setRegion(region)
            self.mapObject.addAnnotation(self.mapLocation!,withPinColor: .Red)
        }
            
        // Home address
        if watchDataExchange.addressName == "Maison"
        {
            mapLocation = CLLocationCoordinate2DMake(watchDataExchange.addressLat,watchDataExchange.addressLong)
            let region = MKCoordinateRegionMake(mapLocation!,span)
            self.mapObject.setRegion(region)
            //self.mapObject.addAnnotation(self.mapLocation!,withPinColor: .Green)
            self.mapObject.addAnnotation(self.mapLocation!, withImageNamed: "housePin@2x.png", centerOffset: CGPoint(x: 0, y: -18))
        }
        
        /*
            WKInterfaceController.openParentApplication(parentValues, reply: { (replyValues, error) -> Void in
            
            let lat = replyValues["latitude"] as! Double
            let long = replyValues["longitude"] as! Double
            
            self.mapLocation = CLLocationCoordinate2DMake(lat, long)
            
            let span = MKCoordinateSpanMake(0.1, 0.1)
            let region = MKCoordinateRegionMake(self.mapLocation!,
                span)
            self.mapObject.setRegion(region)
            self.mapObject.addAnnotation(self.mapLocation!,
                withPinColor: .Red)
        })
    
        */
    
    
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
